from flask import Flask, request
from flask_cors import CORS
import json

app = Flask(__name__)
CORS(app, supports_credentials=True)
app.jinja_env.auto_reload = True
app.config['TEMPLATES_AUTO_RELOAD'] = True

PREFIX = '/server/api'


@app.route("/")
def hello_world():
    return 'hello, world'


@app.route(PREFIX + '/users', methods=['GET'])
def get_user_info():
    return json.dumps({
        'user_id': 10001,
        'username': 'tester',
        'email': 'example@test.com',
    })


@app.route(PREFIX + '/project/project_tree', methods=['GET'])
def get_project_tree():
    return json.dumps(([
        {
            'title': "parent 0",
            'project_id': 1234,
            "project_type": 0,
            'children': [
                {
                    'title': "leaf 0-0",
                    'project_id': 1235,
                    "project_type": 1,
                },
                {
                    'title': "leaf 0-1",
                    'project_id': 1236,
                    "project_type": 1,
                },
            ],
        },
        {
            'title': "parent 1",
            'project_id': 1237,
            "project_type": 0,
            'children': [
                {
                    'title': "leaf 1-0",
                    'project_id': 1238,
                    "project_type": 1,
                },
                {
                    'title': "leaf 1-1",
                    'project_id': 1239,
                    "project_type": 1,
                },
            ],
        },
    ]))


@app.route(PREFIX + '/project/get_project_info', methods=['GET'])
def get_project_info():
    project_id = request.args.get('project_id')
    return json.dumps(({
        'title': '测试项目',
        'project_id': int(project_id),
        "project_type": 1,
    }))


@app.route(PREFIX + '/setting/get_config', methods=['GET'])
def get_config():
    user_id = request.args.get('user_id')
    return json.dumps(({
        "ToolbarAutoHideDelay": 10000,
    }))


@app.route(PREFIX + '/setting/update_config', methods=['POST'])
def update_config():
    data = request.get_json()
    print(data['user_id'], data['ToolbarAutoHideDelay'])
    return json.dumps(({
        "code": 200,
        "message": 'OK',
    }))


###################################################################################

@app.route(PREFIX + '/unit/get_project_units', methods=['GET'])
def get_project_units():
    project_id = request.args.get('project_id')
    print("获取图表 {}".format(project_id))
    return json.dumps(([
        {
            # 图表信息：大小，位置
            'unit_info': {
                'unit_id': 'g10001',
                'data_grid': {'x': 0, 'y': 0, 'w': 3, 'h': 4, 'minW': 1, 'maxW': 12},
            },
            # 图标数据：图形，颜色
            'chart_info': {
                'title': {
                    'text': '测试图表'
                },
                'color': ['#3398DB'],
                'tooltip': {
                    'trigger': 'axis',
                    'axisPointer': {
                        'type': 'shadow'
                    }
                },
                'grid': {
                    'left': '3%',
                    'right': '4%',
                    'bottom': '3%',
                    'containLabel': 'true'
                },
                'xAxis': [
                    {
                        'type': 'category',
                        'data': ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                        'axisTick': {
                            'alignWithLabel': 'true'
                        }
                    }
                ],
                'yAxis': [
                    {
                        'type': 'value'
                    }
                ],
                'series': [
                    {
                        'name': '直接访问',
                        'type': 'bar',
                        'barWidth': '60%',
                        'data': [10, 52, 200, 334, 390, 330, 220]
                    }
                ]
            },
        },
    ]))
